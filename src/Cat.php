<?php

namespace bges\sampleprj;

use Illuminate\Database\Eloquent\Model;

class Cat extends Model
{
    protected $table = 'cats';
    protected $connection = 'mysql2';
}
