<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'bges\sampleprj'], function () {
    Route::get('/', ['uses' => 'TestController@test']);
    Route::get('/cats', ['uses' => 'CatController@getAll']);
    Route::get('/cats/store', ['uses' => 'CatController@store']);
});
